--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.25
-- Dumped by pg_dump version 9.5.25

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: aplicacion; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA aplicacion;


ALTER SCHEMA aplicacion OWNER TO postgres;

--
-- Name: sistema; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA sistema;


ALTER SCHEMA sistema OWNER TO postgres;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: doctores; Type: TABLE; Schema: aplicacion; Owner: hugo
--

CREATE TABLE aplicacion.doctores (
    doctor integer NOT NULL,
    cedula integer,
    nombres character varying
);


ALTER TABLE aplicacion.doctores OWNER TO hugo;

--
-- Name: doctores_doctor_seq; Type: SEQUENCE; Schema: aplicacion; Owner: hugo
--

CREATE SEQUENCE aplicacion.doctores_doctor_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE aplicacion.doctores_doctor_seq OWNER TO hugo;

--
-- Name: doctores_doctor_seq; Type: SEQUENCE OWNED BY; Schema: aplicacion; Owner: hugo
--

ALTER SEQUENCE aplicacion.doctores_doctor_seq OWNED BY aplicacion.doctores.doctor;


--
-- Name: pacientes; Type: TABLE; Schema: aplicacion; Owner: postgres
--

CREATE TABLE aplicacion.pacientes (
    paciente integer NOT NULL,
    cedula integer,
    nombres character varying
);


ALTER TABLE aplicacion.pacientes OWNER TO postgres;

--
-- Name: pacientes_paciente_seq; Type: SEQUENCE; Schema: aplicacion; Owner: postgres
--

CREATE SEQUENCE aplicacion.pacientes_paciente_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE aplicacion.pacientes_paciente_seq OWNER TO postgres;

--
-- Name: pacientes_paciente_seq; Type: SEQUENCE OWNED BY; Schema: aplicacion; Owner: postgres
--

ALTER SEQUENCE aplicacion.pacientes_paciente_seq OWNED BY aplicacion.pacientes.paciente;


--
-- Name: roles; Type: TABLE; Schema: sistema; Owner: postgres
--

CREATE TABLE sistema.roles (
    rol integer NOT NULL,
    nombre_rol character varying(140),
    auxiliar integer
);


ALTER TABLE sistema.roles OWNER TO postgres;

--
-- Name: roles_rol_seq; Type: SEQUENCE; Schema: sistema; Owner: postgres
--

CREATE SEQUENCE sistema.roles_rol_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sistema.roles_rol_seq OWNER TO postgres;

--
-- Name: roles_rol_seq; Type: SEQUENCE OWNED BY; Schema: sistema; Owner: postgres
--

ALTER SEQUENCE sistema.roles_rol_seq OWNED BY sistema.roles.rol;


--
-- Name: roles_x_selectores; Type: TABLE; Schema: sistema; Owner: postgres
--

CREATE TABLE sistema.roles_x_selectores (
    id integer NOT NULL,
    rol integer,
    selector integer
);


ALTER TABLE sistema.roles_x_selectores OWNER TO postgres;

--
-- Name: roles_x_selectores_id_seq; Type: SEQUENCE; Schema: sistema; Owner: postgres
--

CREATE SEQUENCE sistema.roles_x_selectores_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sistema.roles_x_selectores_id_seq OWNER TO postgres;

--
-- Name: roles_x_selectores_id_seq; Type: SEQUENCE OWNED BY; Schema: sistema; Owner: postgres
--

ALTER SEQUENCE sistema.roles_x_selectores_id_seq OWNED BY sistema.roles_x_selectores.id;


--
-- Name: selectores; Type: TABLE; Schema: sistema; Owner: postgres
--

CREATE TABLE sistema.selectores (
    id integer NOT NULL,
    superior integer,
    descripcion character varying,
    ord integer,
    link character varying
);


ALTER TABLE sistema.selectores OWNER TO postgres;

--
-- Name: selectores_id_seq; Type: SEQUENCE; Schema: sistema; Owner: postgres
--

CREATE SEQUENCE sistema.selectores_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sistema.selectores_id_seq OWNER TO postgres;

--
-- Name: selectores_id_seq; Type: SEQUENCE OWNED BY; Schema: sistema; Owner: postgres
--

ALTER SEQUENCE sistema.selectores_id_seq OWNED BY sistema.selectores.id;


--
-- Name: selectores_x_webservice; Type: TABLE; Schema: sistema; Owner: postgres
--

CREATE TABLE sistema.selectores_x_webservice (
    id integer NOT NULL,
    selector integer,
    wservice integer
);


ALTER TABLE sistema.selectores_x_webservice OWNER TO postgres;

--
-- Name: selectores_x_webservice_id_seq; Type: SEQUENCE; Schema: sistema; Owner: postgres
--

CREATE SEQUENCE sistema.selectores_x_webservice_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sistema.selectores_x_webservice_id_seq OWNER TO postgres;

--
-- Name: selectores_x_webservice_id_seq; Type: SEQUENCE OWNED BY; Schema: sistema; Owner: postgres
--

ALTER SEQUENCE sistema.selectores_x_webservice_id_seq OWNED BY sistema.selectores_x_webservice.id;


--
-- Name: usuarios; Type: TABLE; Schema: sistema; Owner: postgres
--

CREATE TABLE sistema.usuarios (
    usuario integer NOT NULL,
    cuenta character varying(100),
    clave character varying(150),
    token_iat character varying
);


ALTER TABLE sistema.usuarios OWNER TO postgres;

--
-- Name: usuarios_usuario_seq; Type: SEQUENCE; Schema: sistema; Owner: postgres
--

CREATE SEQUENCE sistema.usuarios_usuario_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sistema.usuarios_usuario_seq OWNER TO postgres;

--
-- Name: usuarios_usuario_seq; Type: SEQUENCE OWNED BY; Schema: sistema; Owner: postgres
--

ALTER SEQUENCE sistema.usuarios_usuario_seq OWNED BY sistema.usuarios.usuario;


--
-- Name: usuarios_x_roles; Type: TABLE; Schema: sistema; Owner: postgres
--

CREATE TABLE sistema.usuarios_x_roles (
    id integer NOT NULL,
    usuario integer,
    rol integer
);


ALTER TABLE sistema.usuarios_x_roles OWNER TO postgres;

--
-- Name: usuarios_x_roles_id_seq; Type: SEQUENCE; Schema: sistema; Owner: postgres
--

CREATE SEQUENCE sistema.usuarios_x_roles_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sistema.usuarios_x_roles_id_seq OWNER TO postgres;

--
-- Name: usuarios_x_roles_id_seq; Type: SEQUENCE OWNED BY; Schema: sistema; Owner: postgres
--

ALTER SEQUENCE sistema.usuarios_x_roles_id_seq OWNED BY sistema.usuarios_x_roles.id;


--
-- Name: webservice; Type: TABLE; Schema: sistema; Owner: postgres
--

CREATE TABLE sistema.webservice (
    wservice integer NOT NULL,
    path character varying,
    nombre character varying
);


ALTER TABLE sistema.webservice OWNER TO postgres;

--
-- Name: webservice_wservice_seq; Type: SEQUENCE; Schema: sistema; Owner: postgres
--

CREATE SEQUENCE sistema.webservice_wservice_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sistema.webservice_wservice_seq OWNER TO postgres;

--
-- Name: webservice_wservice_seq; Type: SEQUENCE OWNED BY; Schema: sistema; Owner: postgres
--

ALTER SEQUENCE sistema.webservice_wservice_seq OWNED BY sistema.webservice.wservice;


--
-- Name: doctor; Type: DEFAULT; Schema: aplicacion; Owner: hugo
--

ALTER TABLE ONLY aplicacion.doctores ALTER COLUMN doctor SET DEFAULT nextval('aplicacion.doctores_doctor_seq'::regclass);


--
-- Name: paciente; Type: DEFAULT; Schema: aplicacion; Owner: postgres
--

ALTER TABLE ONLY aplicacion.pacientes ALTER COLUMN paciente SET DEFAULT nextval('aplicacion.pacientes_paciente_seq'::regclass);


--
-- Name: rol; Type: DEFAULT; Schema: sistema; Owner: postgres
--

ALTER TABLE ONLY sistema.roles ALTER COLUMN rol SET DEFAULT nextval('sistema.roles_rol_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: sistema; Owner: postgres
--

ALTER TABLE ONLY sistema.roles_x_selectores ALTER COLUMN id SET DEFAULT nextval('sistema.roles_x_selectores_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: sistema; Owner: postgres
--

ALTER TABLE ONLY sistema.selectores ALTER COLUMN id SET DEFAULT nextval('sistema.selectores_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: sistema; Owner: postgres
--

ALTER TABLE ONLY sistema.selectores_x_webservice ALTER COLUMN id SET DEFAULT nextval('sistema.selectores_x_webservice_id_seq'::regclass);


--
-- Name: usuario; Type: DEFAULT; Schema: sistema; Owner: postgres
--

ALTER TABLE ONLY sistema.usuarios ALTER COLUMN usuario SET DEFAULT nextval('sistema.usuarios_usuario_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: sistema; Owner: postgres
--

ALTER TABLE ONLY sistema.usuarios_x_roles ALTER COLUMN id SET DEFAULT nextval('sistema.usuarios_x_roles_id_seq'::regclass);


--
-- Name: wservice; Type: DEFAULT; Schema: sistema; Owner: postgres
--

ALTER TABLE ONLY sistema.webservice ALTER COLUMN wservice SET DEFAULT nextval('sistema.webservice_wservice_seq'::regclass);


--
-- Data for Name: doctores; Type: TABLE DATA; Schema: aplicacion; Owner: hugo
--

COPY aplicacion.doctores (doctor, cedula, nombres) FROM stdin;
1	1	primer doctor
\.


--
-- Name: doctores_doctor_seq; Type: SEQUENCE SET; Schema: aplicacion; Owner: hugo
--

SELECT pg_catalog.setval('aplicacion.doctores_doctor_seq', 1, true);


--
-- Data for Name: pacientes; Type: TABLE DATA; Schema: aplicacion; Owner: postgres
--

COPY aplicacion.pacientes (paciente, cedula, nombres) FROM stdin;
1	123	primer
\.


--
-- Name: pacientes_paciente_seq; Type: SEQUENCE SET; Schema: aplicacion; Owner: postgres
--

SELECT pg_catalog.setval('aplicacion.pacientes_paciente_seq', 2, true);


--
-- Data for Name: roles; Type: TABLE DATA; Schema: sistema; Owner: postgres
--

COPY sistema.roles (rol, nombre_rol, auxiliar) FROM stdin;
1	admin	\N
2	acceso basico	\N
3	rol 3	1
4	rol 4	\N
\.


--
-- Name: roles_rol_seq; Type: SEQUENCE SET; Schema: sistema; Owner: postgres
--

SELECT pg_catalog.setval('sistema.roles_rol_seq', 4, true);


--
-- Data for Name: roles_x_selectores; Type: TABLE DATA; Schema: sistema; Owner: postgres
--

COPY sistema.roles_x_selectores (id, rol, selector) FROM stdin;
1	1	1
2	1	2
3	1	3
4	1	4
5	1	5
6	1	6
8	2	2
11	1	7
12	1	10
14	1	11
\.


--
-- Name: roles_x_selectores_id_seq; Type: SEQUENCE SET; Schema: sistema; Owner: postgres
--

SELECT pg_catalog.setval('sistema.roles_x_selectores_id_seq', 14, true);


--
-- Data for Name: selectores; Type: TABLE DATA; Schema: sistema; Owner: postgres
--

COPY sistema.selectores (id, superior, descripcion, ord, link) FROM stdin;
1	0	Sistema	10	
6	1	Cambio de password	10	/sistema/cambiopass/
4	1	Usuarios	15	/sistema/usuario/
5	1	Roles	20	/sistema/rol/
3	1	Acceso menu	30	/sistema/selector/
2	1	Salir	50	/
7	0	Basicas	20	
10	7	Pacientes	10	/aplicacion/paciente/
11	7	Doctores	20	/aplicacion/doctor/
\.


--
-- Name: selectores_id_seq; Type: SEQUENCE SET; Schema: sistema; Owner: postgres
--

SELECT pg_catalog.setval('sistema.selectores_id_seq', 11, true);


--
-- Data for Name: selectores_x_webservice; Type: TABLE DATA; Schema: sistema; Owner: postgres
--

COPY sistema.selectores_x_webservice (id, selector, wservice) FROM stdin;
\.


--
-- Name: selectores_x_webservice_id_seq; Type: SEQUENCE SET; Schema: sistema; Owner: postgres
--

SELECT pg_catalog.setval('sistema.selectores_x_webservice_id_seq', 1, false);


--
-- Data for Name: usuarios; Type: TABLE DATA; Schema: sistema; Owner: postgres
--

COPY sistema.usuarios (usuario, cuenta, clave, token_iat) FROM stdin;
1	admin	202cb962ac59075b964b07152d234b70	1677463231831
2	usuario 2	5e543256c480ac577d30f76f9120eb74	1677034946399
3	usuario 3	5e543256c480ac577d30f76f9120eb74	\N
\.


--
-- Name: usuarios_usuario_seq; Type: SEQUENCE SET; Schema: sistema; Owner: postgres
--

SELECT pg_catalog.setval('sistema.usuarios_usuario_seq', 3, true);


--
-- Data for Name: usuarios_x_roles; Type: TABLE DATA; Schema: sistema; Owner: postgres
--

COPY sistema.usuarios_x_roles (id, usuario, rol) FROM stdin;
1	1	1
2	2	2
3	2	3
\.


--
-- Name: usuarios_x_roles_id_seq; Type: SEQUENCE SET; Schema: sistema; Owner: postgres
--

SELECT pg_catalog.setval('sistema.usuarios_x_roles_id_seq', 3, true);


--
-- Data for Name: webservice; Type: TABLE DATA; Schema: sistema; Owner: postgres
--

COPY sistema.webservice (wservice, path, nombre) FROM stdin;
\.


--
-- Name: webservice_wservice_seq; Type: SEQUENCE SET; Schema: sistema; Owner: postgres
--

SELECT pg_catalog.setval('sistema.webservice_wservice_seq', 1, false);


--
-- Name: doctores_cedula_key; Type: CONSTRAINT; Schema: aplicacion; Owner: hugo
--

ALTER TABLE ONLY aplicacion.doctores
    ADD CONSTRAINT doctores_cedula_key UNIQUE (cedula);


--
-- Name: doctores_pkey; Type: CONSTRAINT; Schema: aplicacion; Owner: hugo
--

ALTER TABLE ONLY aplicacion.doctores
    ADD CONSTRAINT doctores_pkey PRIMARY KEY (doctor);


--
-- Name: pacientes_cedula_key; Type: CONSTRAINT; Schema: aplicacion; Owner: postgres
--

ALTER TABLE ONLY aplicacion.pacientes
    ADD CONSTRAINT pacientes_cedula_key UNIQUE (cedula);


--
-- Name: pacientes_pkey; Type: CONSTRAINT; Schema: aplicacion; Owner: postgres
--

ALTER TABLE ONLY aplicacion.pacientes
    ADD CONSTRAINT pacientes_pkey PRIMARY KEY (paciente);


--
-- Name: roles_pkey; Type: CONSTRAINT; Schema: sistema; Owner: postgres
--

ALTER TABLE ONLY sistema.roles
    ADD CONSTRAINT roles_pkey PRIMARY KEY (rol);


--
-- Name: roles_x_selectores_pkey; Type: CONSTRAINT; Schema: sistema; Owner: postgres
--

ALTER TABLE ONLY sistema.roles_x_selectores
    ADD CONSTRAINT roles_x_selectores_pkey PRIMARY KEY (id);


--
-- Name: selectores_pkey; Type: CONSTRAINT; Schema: sistema; Owner: postgres
--

ALTER TABLE ONLY sistema.selectores
    ADD CONSTRAINT selectores_pkey PRIMARY KEY (id);


--
-- Name: selectores_x_webservice_pkey; Type: CONSTRAINT; Schema: sistema; Owner: postgres
--

ALTER TABLE ONLY sistema.selectores_x_webservice
    ADD CONSTRAINT selectores_x_webservice_pkey PRIMARY KEY (id);


--
-- Name: usuarios_pkey; Type: CONSTRAINT; Schema: sistema; Owner: postgres
--

ALTER TABLE ONLY sistema.usuarios
    ADD CONSTRAINT usuarios_pkey PRIMARY KEY (usuario);


--
-- Name: usuarios_x_roles_pkey; Type: CONSTRAINT; Schema: sistema; Owner: postgres
--

ALTER TABLE ONLY sistema.usuarios_x_roles
    ADD CONSTRAINT usuarios_x_roles_pkey PRIMARY KEY (id);


--
-- Name: webservice_pkey; Type: CONSTRAINT; Schema: sistema; Owner: postgres
--

ALTER TABLE ONLY sistema.webservice
    ADD CONSTRAINT webservice_pkey PRIMARY KEY (wservice);


--
-- Name: roles_x_selectores_rol_fkey; Type: FK CONSTRAINT; Schema: sistema; Owner: postgres
--

ALTER TABLE ONLY sistema.roles_x_selectores
    ADD CONSTRAINT roles_x_selectores_rol_fkey FOREIGN KEY (rol) REFERENCES sistema.roles(rol);


--
-- Name: roles_x_selectores_selector_fkey; Type: FK CONSTRAINT; Schema: sistema; Owner: postgres
--

ALTER TABLE ONLY sistema.roles_x_selectores
    ADD CONSTRAINT roles_x_selectores_selector_fkey FOREIGN KEY (selector) REFERENCES sistema.selectores(id);


--
-- Name: usuarios_x_roles_rol_fkey; Type: FK CONSTRAINT; Schema: sistema; Owner: postgres
--

ALTER TABLE ONLY sistema.usuarios_x_roles
    ADD CONSTRAINT usuarios_x_roles_rol_fkey FOREIGN KEY (rol) REFERENCES sistema.roles(rol);


--
-- Name: usuarios_x_roles_usuario_fkey; Type: FK CONSTRAINT; Schema: sistema; Owner: postgres
--

ALTER TABLE ONLY sistema.usuarios_x_roles
    ADD CONSTRAINT usuarios_x_roles_usuario_fkey FOREIGN KEY (usuario) REFERENCES sistema.usuarios(usuario);


--
-- Name: SCHEMA public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

