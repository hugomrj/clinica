

function Doctor(){
    
   this.tipo = "doctor";   
   this.recurso = "doctores";   
   this.value = 0;
   
   
   this.dom="";
   this.carpeta=  "/aplicacion";   
   
   
   this.titulosin = "Doctor"
   this.tituloplu = "Doctores"   
      
   
   this.campoid=  'doctor';
   this.tablacampos =  ['doctor', 'cedula', 'nombres' ];
   
   this.etiquetas =  ['Codigo', 'Cedula' , 'Nombre' ];
    
   this.tablaformat = ['N', 'N' , 'C' ];                                  
   
   this.tbody_id = "doctor-tb";
      
   this.botones_lista = [ this.lista_new] ;
   this.botones_form = "doctor-acciones";   
         
   this.parent = null;
   
   this.filtro = "";
   
}





Doctor.prototype.new = function( obj  ) {                

    reflex.form_new( obj ); 
    reflex.acciones.button_add_promise(obj);        
    
    
    
    var cedula = document.getElementById('doctor_cedula');            
    cedula.focus();
    cedula.select();                
    
};





Doctor.prototype.form_ini = function() {    
  
  
    var cedula = document.getElementById('doctor_cedula');            
     cedula.onblur  = function() {                  
         cedula.value = fmtNum(cedula.value);      
    };      
    cedula.onblur();          
  
    
};






Doctor.prototype.form_validar = function() {    
    
   
    var cedula = document.getElementById('doctor_cedula');        
    if (parseInt(NumQP(cedula.value)) <= 0 )         
    {
        msg.error.mostrar("Falta numero de cedula");    
        cedula.focus();
        cedula.select();                
        return false;
    }        
       
    
    
    var nombres = document.getElementById('doctor_nombres');    
    if (nombres.value == "")         
    {
        msg.error.mostrar("Falta nombre y apellido" );           
        nombres.focus();
        nombres.select();        
        return false;
    }       
        

    
    return true;
};










Doctor.prototype.main_list = function(obj, page) {    


    if (page === undefined) {    
        page = 1;
    }

    let promesa = arasa.vista.lista_paginacion(obj, page);
    
    promesa        
        .then(( xhr ) => {              
            arasa.html.url.redirect(xhr.status);                                                          
            // botones de accion - nuevo para este caso
            
            // botones de accion - nuevo para este caso       
            boton.objeto = ""+obj.tipo;
            document.getElementById( obj.tipo +'_acciones_lista' ).innerHTML 
                =  boton.basicform.get_botton_new();                
            
            var btn_objeto_nuevo = document.getElementById('btn_' + obj.tipo + '_nuevo');
            btn_objeto_nuevo.addEventListener('click',
                function(event) {  
                    
                    obj.new( obj );

                },
                false
            );              
            
           

        })
        .catch(( xhr ) => { 
            console.log(xhr.message);
        }); 

};








Doctor.prototype.icobusqueda = function( obj  ) {      
    
    html.topbar.icobusqueda(obj);    

}





Doctor.prototype.getUrlFiltro = function( obj  ) {                    
    
    var ret = "";    
      
    ret = obj.filtro;
   

    return ret;
    
};


