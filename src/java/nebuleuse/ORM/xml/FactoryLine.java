/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nebuleuse.ORM.xml;

/**
 *
 * @author hugo
 */
public class FactoryLine {
    
    private String linea;
    private String nivel;
    private Character cierre;
    

    public String getLinea() {
        return linea;
    }

    public void setLinea(String linea) {
        this.linea = linea;
    }

    public Character getCierre() {
        return cierre;
    }

    public void setCierre(Character cierre) {
        this.cierre = cierre;
    }

    
    public String getNivel() {
        return nivel;
    }

    public void setNivel(String nivel) {
        this.nivel = nivel;
    }


    
}


