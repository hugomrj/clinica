/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package nebuleuse.ORM.xml;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import nebuleuse.ORM.db.Atributo;
import nebuleuse.ORM.db.Nexo;

import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.NamedNodeMap;




public final  class SerializacionXML {    

        
        //private String ruta =  this.getClass().getResource("/").getPath().replaceAll("classes", "xml");
        private String path =  this.setResoursePath();

        private ArrayList <Nexo> elementos = new ArrayList <Nexo>() ;
        
        
 /*
        private ArrayList <Nexo> elementos = new ArrayList <Nexo>()  {
            { 
                add(new Nexo());  
                add(new Nexo());
            }
        };
   */
        
        
    public SerializacionXML() {            

    }


    public SerializacionXML( Object objeto)  {
        this.generar(objeto) ;        
    }
        
        
        
        
        
    public String setResoursePath ( )  {
        
        String str =  "";
        
        str  = this.getClass().getResource(this.getClass().getSimpleName() + ".class").getPath();
        
        str = str.substring(0, str.indexOf("WEB-INF") + "WEB-INF".length() ) ;
        
        str = str  +"/xml/";        
        
        this.path = str;
       
        return str;
     
    }             
                  
        
        
        
        
        public void generar( Object objeto)   {
        
            
            String xmlfile = objeto.getClass().getSimpleName();
            String ext = xmlfile.substring(xmlfile.length()-3, xmlfile.length());
                 
            
            if (ext.equals("Ext") ){
                xmlfile = xmlfile.substring(0, xmlfile.length() - 3);;
            }

            this.setResoursePath();
            File fileXML = new File(this.path + xmlfile + ".xml");              

            if (fileXML.exists())
            {
                FactoryXML factory = new FactoryXML();                    
                factory.readfile(fileXML);

                DocXML docxml = new DocXML();

                docxml.normalizeDoc(factory);

                this.recorrerNodo(docxml, objeto);      
    
            }
            else
            {
                System.out.println((this.path+objeto.getClass().getSimpleName()+".xml"));
                System.out.println("No existe archivo xml");
            } 
        }  

        
        
        
    public void recorrerNodo (DocXML xml, Object objeto)  {
        
        
   
        ArrayList<NodoXML> hijos = xml.getNodos();
                               
        Nexo elemento = new Nexo();
        
        boolean isElemento = false;
                
        this.elementos.add(new Nexo());
        this.elementos.add(new Nexo());


        for ( int i = 0; i < hijos.size()  ; i++ )
        {
                        
            NodoXML nodo = hijos.get(i);
            
   

            if (nodo.getNodeType() == Node.ELEMENT_NODE) 
            //if (true) 
            { 
                
                ArrayList<Atributo> attributesList = nodo.getAtributos();
                
                ////////
                ArrayList<Atributo> listaAtributos = new ArrayList<Atributo>();   
                
                for (int j = 0; j < attributesList.size(); j++) 
                {
                    
                    // se carga el nombre de la tabla y el indice y las demas columnas                                                 
                    if (nodo.getNombre().equals("table"))
                    {                        
                        if (attributesList.get(j).getNombre().equals("name"))
                        {                                                       
                            
                            this.getElementos().get(0).setTabla(attributesList.get(j).getValor() );
                            this.getElementos().get(0).setObjeto(objeto.getClass().getSimpleName());                                
                        }
                    }                                
                    else if ((nodo.getNombre().equals("id"))) 
                    {            
                        
                        if (attributesList.get(j).getNombre().equals("name"))
                        {
                            // se copia el nombre del campo y la propiedad iguales
                            this.getElementos().get(1).setTabla(  attributesList.get(j).getValor());                                                                
                            this.getElementos().get(1).setObjeto( attributesList.get(j).getValor());                                                                
                        }
                        else if (attributesList.get(j).getNombre().equals("property"))
                        {   
                            // se copia la propiedad en el caso que exista                                                                                         
                            this.getElementos().get(1).setObjeto( attributesList.get(j).getValor());                                                                
                        }
                        
                    }                        
                    // recorre las columnas            
                    else if ((nodo.getNombre().equals("column"))) 
                    {    
                        

                        if (attributesList.get(j).getNombre().equals("name"))
                        {       
                            
                            // se copia el nombre del campo y la propiedad iguales
                            elemento.setTabla(attributesList.get(j).getValor());
                            elemento.setObjeto(attributesList.get(j).getValor());
                            isElemento = true;                            
                        }// fin if name
                        
                        
                        else if (attributesList.get(j).getNombre().equals("property"))
                        {
                            // se copia la propiedad en el caso que exista                                                            
                            elemento.setObjeto(attributesList.get(j).getValor());
                            isElemento = true;
                        }

                        else if (attributesList.get(j).getNombre().equals("foreign"))
                        {
                            // se copia la propiedad en el caso que exista                                                             
                            //elemento.setObjeto(attributesList.item(j).getNodeValue());
                    
                           listaAtributos.add(new Atributo("foreing", attributesList.get(j).getValor()));
                           isElemento = true;             
                        }
                        
                        else if (attributesList.get(j).getNombre().equals("updatenull"))
                        {
                            // se copia la propiedad en el caso que exista                                                             
                            //elemento.setObjeto(attributesList.item(j).getNodeValue());

                           listaAtributos.add(new Atributo("updatenull", attributesList.get(j).getValor()));
                           isElemento = true;             
                        }
                        
                        else if (attributesList.get(j).getNombre().equals("insertnot"))
                        {
                           listaAtributos.add(new Atributo("insertnot", attributesList.get(j).getValor()));
                           isElemento = true;             
                        }
                        
                        else if (attributesList.get(j).getNombre().equals("selectnot"))
                        {
                           listaAtributos.add(new Atributo("selectnot", attributesList.get(j).getValor()));
                           isElemento = true;             
                        }
                        
                        
                    }      
                    else 
                    {                                             
                        System.out.println(" No se encontro etiqueta xml  "+nodo.getNombre());    
                    }  
                }

                
                
                
                // agreaga elemento en la lista de elementos
                if ((isElemento == true))
                {
                    Nexo nvoEnlace = new Nexo(elemento.getTabla(), elemento.getObjeto() );
                    nvoEnlace.setAtributo(listaAtributos);
                  
                    this.getElementos().add(nvoEnlace);         
                    
                }                    
                
                
                
                
                /*
                // llamada a recursividad
                if ( nodo.hasChildNodes() == true)
                {
                    recorrerNodo(nodo, objeto);
                }    
                */
                
                
            }
        }  
        
    }
        
/*   
    public String getRuta() {
        return ruta;
    }

    public void setRuta(String ruta) {
        this.ruta = ruta;
    }
*/
    
    
    public ArrayList <Nexo> getElementos() {
        return elementos;
    }

    public void setElementos(ArrayList <Nexo> elementos) {
        this.elementos = elementos;
    }
   
}
    
        
   


