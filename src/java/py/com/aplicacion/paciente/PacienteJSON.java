/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.aplicacion.paciente;


import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import java.io.IOException;
import java.sql.ResultSet;
import nebuleuse.ORM.db.JsonObjeto;
import nebuleuse.ORM.db.ResultadoSet;
import nebuleuse.ORM.sql.SentenciaSQL;


public class PacienteJSON  {


    
    
    public PacienteJSON ( ) throws IOException  {
    
    }
      
    
    
    

    public JsonObject  lista ( Integer page, String buscar ) {
        
        
        JsonObject jsonObject = new JsonObject();

        
        try 
        {   
  

            ResultadoSet resSet = new ResultadoSet();          
            
            
            String sql = "";
            String sqlFiltro = "";            
            String sqlOrder = "";
            
            if (buscar == null) {                
                sql = SentenciaSQL.select(new Paciente());    
            }
            else{                
                sql =  SentenciaSQL.select(new Paciente());   
                sqlFiltro =  new PacienteSQL().filtro(buscar);   ;       
            }        
            
            sqlOrder =  " order by paciente";
            
            sql = sql + sqlFiltro + sqlOrder ;
            
            
            ResultSet rsData = resSet.resultset(sql, page);                
            
            JsonArray jsonarrayDatos = new JsonArray();
            jsonarrayDatos = new JsonObjeto().array_datos(rsData);
            
            
            // paginacipon
            JsonObject jsonPaginacion = new JsonObject();            
            jsonPaginacion = new JsonObjeto().json_paginacion(sql, page);
            
            
            // union de partes
            jsonObject.add("paginacion", jsonPaginacion);
            
            jsonObject.add("datos", jsonarrayDatos);    
            //jsonObject.add("summary", jsonarraySuma);            
            
     
        }         
        catch (Exception ex) {                   
            
            System.out.println(ex.getMessage());
            throw new Exception(ex);
        }
        finally
        {
            return jsonObject ;         
        }
    }      
    
    
    
        
    
    

        
    
    
    
        
}
