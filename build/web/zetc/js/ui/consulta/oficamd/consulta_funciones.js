



function main_form (obj) {    


    fetch( html.url.absolute() + '/consulta/oficamd/htmf/cab.html' )
      .then(response => {
        return response.text();
      })
      .then(data => {
        
        document.getElementById('arti_form').innerHTML = data ;
        
            main_lista(obj);
            
      })

};







function main_lista (obj) {    

    fetch( html.url.absolute() + '/consulta/oficamd/htmf/det.html' )
      .then(response => {
        return response.text();
      })
      .then(data => {
        
        document.getElementById('main_lista').innerHTML = data ;
        
        form_ini (obj);
            
      })

};






function form_ini (obj) {    

    // boton enviar
    var btn_cab_buscar = document.getElementById('btn_cab_buscar');
    btn_cab_buscar.onclick = function(event) {     

        var fecha_desde = document.getElementById('fecha_desde');
        var fecha_hasta = document.getElementById('fecha_hasta');

        if (fecha_desde.value != "" &&  fecha_hasta.value != "" ){        
            Consulta_detalle_promesa( obj  );
        }
        
    }
    

    
    
    
    

    var btn_xlsx = document.getElementById( 'btn_xlsx');
    btn_xlsx.onclick = function()
    {  


        var fecha_desde = document.getElementById('fecha_desde');
        var fecha_hasta = document.getElementById('fecha_hasta');

        if (fecha_desde.value != "" &&  fecha_hasta.value != "" ){        

            loader.inicio();

            var url = html.url.absolute() + '/api/beneficiarios_qry/consulta1/xlsx/'                
                +';fe1='+document.getElementById("fecha_desde").value 
                +';fe2='+document.getElementById("fecha_hasta").value 


            // Use XMLHttpRequest instead of Jquery $ajax
            xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function() {

                if (xhttp.readyState === 4 && xhttp.status === 200) {

                    var file_name = "beneficiarios_detalle.xlsx" ; 

                    // Trick for making downloadable link
                    var a = document.createElement('a');
                    a.href = window.URL.createObjectURL(xhttp.response);
                    a.download = file_name;
                    a.click();
                    loader.fin();

                }
            };
            // Post data to URL which handles post request
            xhttp.open("GET", url);
            xhttp.setRequestHeader("Content-Type", "application/json");

            // You should set responseType as blob for binary responses
            xhttp.responseType = 'blob';
            xhttp.setRequestHeader("token", localStorage.getItem('token'));           
            xhttp.send();

        }
        
        

    }

            
    
          
};







function Consulta_detalle_promesa( obj ) {    


    const promise = new Promise((resolve, reject) => {

            loader.inicio();            

            var url = html.url.absolute() + '/api/beneficiarios_qry/consulta1/'                
                +';fe1='+document.getElementById("fecha_desde").value 
                +';fe2='+document.getElementById("fecha_hasta").value 
                

            var xhr =  new XMLHttpRequest();      

            var metodo = "GET";                                     
            xhr.open( metodo.toUpperCase(),   url,  true );      

            xhr.onreadystatechange = function () {
                if (this.readyState == 4 ){

                    tabla.json = xhr.responseText;




                    var ojson = JSON.parse( tabla.json ) ; 
                    tabla.json = JSON.stringify(ojson['datos']) ;  
                    
                    
                    
                   
                    if (tabla.json != "[]"){
                    
                        tabla.ini(obj);                            
                        tabla.gene();                                  
                        tabla.formato(obj);                            
                                                
                        
                        // suma de consultas
                        var jsumas = JSON.stringify(ojson['summary'][0]) ;              
                        var ojsumas = JSON.parse(jsumas) ;

                        
                        document.getElementById("sum_sexo_masc").innerHTML
                            =  fmtNum( ojsumas["sexo_masc"] );
                        
                        document.getElementById("sum_sexo_feme").innerHTML
                            =  fmtNum( ojsumas["sexo_feme"] );
                        
                        document.getElementById("sum_sexo_otro").innerHTML
                            =  fmtNum( ojsumas["sexo_otro"] );
                        
                        document.getElementById("sum_sexo_nocon").innerHTML
                            =  fmtNum( ojsumas["sexo_nocon"] );
                        
                        document.getElementById("sum_edad_5_12").innerHTML
                            =  fmtNum( ojsumas["edad_5_12"] );
                        
                        document.getElementById("sum_edad_13_17").innerHTML
                            =  fmtNum( ojsumas["edad_13_17"] );
                        
                        document.getElementById("sum_edad_18_29").innerHTML
                            =  fmtNum( ojsumas["edad_18_29"] );
                        
                        document.getElementById("sum_edad_30_49").innerHTML
                            =  fmtNum( ojsumas["edad_30_49"] );
                        
                        document.getElementById("sum_edad_50_64").innerHTML
                            =  fmtNum( ojsumas["edad_50_64"] );
                        
                        document.getElementById("sum_edad_65_mas").innerHTML
                            =  fmtNum( ojsumas["edad_65_mas"] );
                        
                        document.getElementById("sum_edad_nocon").innerHTML
                            =  fmtNum( ojsumas["edad_nocon"] );
                        
                        document.getElementById("sum_zona_1").innerHTML
                            =  fmtNum( ojsumas["zona_1"] );
                        
                        document.getElementById("sum_zona_2").innerHTML
                            =  fmtNum( ojsumas["zona_2"] );
                        
                        document.getElementById("sum_zona_3").innerHTML
                            =  fmtNum( ojsumas["zona_3"] );
                        
                        document.getElementById("sum_zona_4").innerHTML
                            =  fmtNum( ojsumas["zona_4"] );
                        
                        document.getElementById("sum_zona_5").innerHTML
                            =  fmtNum( ojsumas["zona_5"] );
                        
                        document.getElementById("sum_zona_6").innerHTML
                            =  fmtNum( ojsumas["zona_6"] );
                        
                        document.getElementById("sum_zona_7").innerHTML
                            =  fmtNum( ojsumas["zona_7"] );
                        
                        document.getElementById("sum_zona_8").innerHTML
                            =  fmtNum( ojsumas["zona_8"] );
                        
                        document.getElementById("sum_zona_9").innerHTML
                            =  fmtNum( ojsumas["zona_9"] );
                    
                        document.getElementById("sum_zona_10").innerHTML
                            =  fmtNum( ojsumas["zona_10"] );                    
                        
                        document.getElementById("sum_zona_hovy").innerHTML
                            =  fmtNum( ojsumas["zona_hovy"] );
                        
                        document.getElementById("sum_zona_sen").innerHTML
                            =  fmtNum( ojsumas["zona_sen"] );
                        
                        document.getElementById("sum_zona_otro").innerHTML
                            =  fmtNum( ojsumas["zona_otro"] );
                        
                        
                        document.getElementById("sum_acti_quejas").innerHTML
                            =  fmtNum( ojsumas["acti_quejas"] );
                        
                        document.getElementById("sum_acti_acompa").innerHTML
                            =  fmtNum( ojsumas["acti_acompa"] );
                        
                        document.getElementById("sum_acti_info").innerHTML
                            =  fmtNum( ojsumas["acti_info"] );
                        
                        document.getElementById("sum_acti_interme").innerHTML
                            =  fmtNum( ojsumas["acti_interme"] );
                        
                        document.getElementById("sum_acti_otro").innerHTML
                            =  fmtNum( ojsumas["acti_otro"] );
                        
                        

//console.log(ojsumas[0]["sexo_masc"])                                                
                        
                        
                        /*
                        document.getElementById("sum_pagado").innerHTML
                            =  fmtNum( ojsumas["pagado"] );
                        */
                        
                    }
                    else{
                       
                        main_lista (obj);
                       /*
                        fetch( html.url.absolute() + obj.carpeta +'/'+ obj.tipo + '/htmf/det.html' )
                          .then(response => {
                            return response.text();
                          })
                          .then(data => {
                            document.getElementById('det_form').innerHTML = data ;
                            msg.error.mostrar("No existen registros");
                          })                       
                       */
                        
                    }

                    

  
                         resolve( xhr );

                    loader.fin();
                }
            };
            xhr.onerror = function (e) {                    
                    reject(
                        xhr.status,
                        xhr.response   
                    );                 

            };                       

            var type = "application/json";
            xhr.setRequestHeader('Content-Type', type);   
            xhr.setRequestHeader("token", localStorage.getItem('token'));           
            xhr.send( null );                       


    })

    return promise;






};








