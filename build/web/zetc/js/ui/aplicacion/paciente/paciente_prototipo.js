

function Paciente(){
    
   this.tipo = "paciente";   
   this.recurso = "pacientes";   
   this.value = 0;
   
   
   this.dom="";
   this.carpeta=  "/aplicacion";   
   
   
   this.titulosin = "Paciente"
   this.tituloplu = "Pacientes"   
      
   
   this.campoid=  'paciente';
   this.tablacampos =  ['paciente', 'cedula', 'nombres' ];
   
   this.etiquetas =  ['Codigo', 'Cedula' , 'Nombre' ];
    
   this.tablaformat = ['N', 'N' , 'C' ];                                  
   
   this.tbody_id = "paciente-tb";
      
   this.botones_lista = [ this.lista_new] ;
   this.botones_form = "paciente-acciones";   
         
   this.parent = null;
   
   this.filtro = "";
   
}





Paciente.prototype.new = function( obj  ) {                

    reflex.form_new( obj ); 
    reflex.acciones.button_add_promise(obj);        
    
    
    
    var paciente_cedula = document.getElementById('paciente_cedula');            
    paciente_cedula.focus();
    paciente_cedula.select();                
    
};





Paciente.prototype.form_ini = function() {    
  
  
    var paciente_cedula = document.getElementById('paciente_cedula');            
     paciente_cedula.onblur  = function() {                  
         paciente_cedula.value = fmtNum(paciente_cedula.value);      
    };      
    paciente_cedula.onblur();          
  
    
};






Paciente.prototype.form_validar = function() {    
    
   
    var paciente_cedula = document.getElementById('paciente_cedula');        
    if (parseInt(NumQP(paciente_cedula.value)) <= 0 )         
    {
        msg.error.mostrar("Falta numero de cedula");    
        paciente_cedula.focus();
        paciente_cedula.select();                
        return false;
    }        
       
    
    
    var paciente_nombres = document.getElementById('paciente_nombres');    
    if (paciente_nombres.value == "")         
    {
        msg.error.mostrar("Falta nombre y apellido" );           
        paciente_nombres.focus();
        paciente_nombres.select();        
        return false;
    }       
        

    
    return true;
};










Paciente.prototype.main_list = function(obj, page) {    


    if (page === undefined) {    
        page = 1;
    }

    let promesa = arasa.vista.lista_paginacion(obj, page);
    
    promesa        
        .then(( xhr ) => {              
            arasa.html.url.redirect(xhr.status);                                                          
            // botones de accion - nuevo para este caso
            
            // botones de accion - nuevo para este caso       
            boton.objeto = ""+obj.tipo;
            document.getElementById( obj.tipo +'_acciones_lista' ).innerHTML 
                =  boton.basicform.get_botton_new();                
            
            var btn_objeto_nuevo = document.getElementById('btn_' + obj.tipo + '_nuevo');
            btn_objeto_nuevo.addEventListener('click',
                function(event) {  
                    
                    obj.new( obj );

                },
                false
            );              
            
           

        })
        .catch(( xhr ) => { 
            console.log(xhr.message);
        }); 

};








Paciente.prototype.icobusqueda = function( obj  ) {      
    
    html.topbar.icobusqueda(obj);    

}





Paciente.prototype.getUrlFiltro = function( obj  ) {                    
    
    var ret = "";    
      
    ret = obj.filtro;
   

    return ret;
    
};


