
var html = 
{      
    sitio:  "",
    
    menu: {  
        
        user: 0,
        div: "menubar",

        ini: function( modsis ) {
            
     

            ajax.url = html.url.absolute() + "/api/selectores/menu";           
            ajax.metodo = "GET";     
            html.menu.user = ajax.private.json(null);
            
//console.log(ajax.req.getResponseHeader("menu"));           
            var mn = ajax.req.getResponseHeader("menu");
            mn = mn.replace("'/webapp/'", "'/webapp/"+modsis +"'" );
            

            //localStorage.setItem('menu', ajax.req.getResponseHeader("menu") );    
            localStorage.setItem('menu', mn );    
            
        
        
            
        }  ,                                



        mostrar: function(  ) {            

            
             var nomsis = html.url.absolute();
             
             if (nomsis != "/webapp"){
                 tmpmenu = localStorage.getItem('menu'); 
                 tmpmenu = tmpmenu.replaceAll('/webapp', nomsis);
                 
                 localStorage.setItem('menu', tmpmenu );     
             }
             
                
            
            document.getElementById( html.menu.div ).innerHTML 
                    =  localStorage.getItem('menu');                    

            
            //  logo   header  <img src=\"../../ima/header/01.png\"/>
            
            document.getElementById( "header_logo" ).innerHTML  =
                    "<div></div>\n\
                    <div></div>\n\
                    <div></div>";   
        }  ,                        
        
        
        
        
    },
    
    url:{
        
        control: function(  fn ) {
            
            loader.inicio();
            
            var comp = window.location.pathname;            
            var carp =  html.url.absolute();
            var path =   comp.replace(carp, "");       
            
            xhr =  new XMLHttpRequest();            
            var url = html.url.absolute()+ "/api/usuarios/controlurl"; 
            var metodo = "GET";     

            xhr.open( metodo.toUpperCase(),   url,  true );      
        
            xhr.onreadystatechange = function () {
                if (this.readyState == 4 ){

                    //ajax.headers.get();
                    ajax.local.token =  xhr.getResponseHeader("token") ;            
                    localStorage.setItem('token', xhr.getResponseHeader("token") );     
      
                    ajax.state = xhr.status;
                    
                    html.url.redirect(ajax.state);   
                    
                    fn();
                    
                    loader.fin();
                    
                    
                    // cabecera basica
                    //html.barra_superior();


                }
            };
            xhr.onerror = function (e) {  
              alert("error");
            };         
      
            xhr.setRequestHeader("path", path );
            var type = "application/json";
            xhr.setRequestHeader('Content-Type', type);   
            //ajax.headers.set();
            xhr.setRequestHeader("token", localStorage.getItem('token'));           
      
            xhr.send( null );                                            
            
        } ,     
        
               
        
        
        
        absolute: function() 
        {
            var pathname = window.location.pathname;
            
            
            pathname = pathname.substring(1,pathname.length);
            pathname = pathname.substring(0,pathname.indexOf("/"));
            return "/"+pathname ;            
        },        
                
                
                
        redirect: function(val) 
        {
            
            switch(val) {
                case 200:
                    break;                    
                case 401:
                    window.location = html.url.absolute() ;                                     
                case 500:
                    window.location = html.url.absolute() ;                 
                    break;            
                default:
                    window.location = html.url.absolute() ;                 
            }

        },                  
                
        
    },
    
    
    barra_superior: function(  ) {  


        ajax.url =   html.url.absolute()+ '/basicas/barrasuperior/barrasuperior.html';   
        ajax.metodo = "GET";            
        document.getElementById( "barrasuperior" ).innerHTML =  ajax.public.html();        
        
        document.getElementById( "session_usuario_cab" ).innerHTML = 
                xhr.getResponseHeader("sessionusuario");

        
        document.getElementById( "session_sucursal_cab" ).innerHTML = 
                xhr.getResponseHeader("sessionsucursal");    
    
    
    },

    topbar:{


        icobusqueda: function( obj ) {            
            // cuadro de busqueda            
            fetch(  html.url.absolute() + '/com/busqueda/busqueda_bar.html' )
              .then(response => {
                return response.text();
              })
              .then(data => {
                document.getElementById( "bar_busqueda" ).innerHTML =  data;                    
                html.topbar.busqueda_bar_accion(obj);
              })   
        }  ,                        


        busqueda_bar_accion: function( obj ) { 
            
            var search_bar = document.getElementById( "icon-search_bar" );
            search_bar.addEventListener('click',
                function(event) {      


                    var b1 = document.getElementById( "icon-search_bar" ); 
                    b1.style.visibility = "hidden";        
                    b1.parentNode.style.display = "none";     

                    var b2 = document.getElementById( "icon-up_bar" ); 
                    b2.style.visibility = "visible";        
                    b2.parentNode.style.display = "block";     



                    // cuadro de busqueda            
                    fetch( html.url.absolute() +  '/com/busqueda/busqueda.html' )
                      .then(response => {
                        return response.text();
                      })
                      .then(data => {
                        document.getElementById( "divbusqueda" ).innerHTML =  data;            

                        var ico_x  =  document.getElementById( "ico-x" )
                        ico_x.onclick = function()
                        {   
                            document.getElementById("qry_busquedatexto").value = "";
                            ico_x.style.visibility = "hidden";                                 
                        }                        
                        
                        
                        
                        
                        // funcion js
                        var qry_busquedatexto = document.getElementById("qry_busquedatexto");
                        qry_busquedatexto.addEventListener("keyup", function(event) {                      
                                
                            if ((this.value.length) > 0)  {                                
                                 ico_x.style.visibility = "visible";                                 
                            }
                            else{
                                 ico_x.style.visibility = "hidden";                                 
                            }

                            
                            
                            
                            if (event.keyCode === 13) {                      
                                event.preventDefault();     

                                var tex = this.value;                            
                                obj.filtro = ';q=' + tex ;
                                obj.main_list(obj, 1); 
                            
                            }
                        });



                        var idcompuest = document.getElementById("idcompuest");   
                        idcompuest.style.display="none";
                        /*
                        idcompuest.onclick = function()
                        {                                  
                            document.getElementById("busqueda_simple").style.display="none";                       

                            fetch( html.url.absolute() +   '/contrataciones/codigocontratacion/'+ '/htmf/busqueda_compuesta.html' )
                              .then(response => {
                                return response.text();
                              })
                              .then(data => {

                                document.getElementById("bus_com_id").style.display="inherit";                           
                                document.getElementById( "bus_com_id" ).innerHTML =  data;    

                                form_busqueda_compuesta( obj );

                              })        
                        };        
                        */

                      })            


                },
                false
            );     




            var up_bar = document.getElementById( "icon-up_bar" );
            up_bar.addEventListener('click',
                function(event) {      

                    var b1 = document.getElementById( "icon-search_bar" ); 
                    b1.style.visibility = "visible";        
                    b1.parentNode.style.display = "block";  

                    var b2 = document.getElementById( "icon-up_bar" ); 
                    b2.style.visibility = "hidden";        
                    b2.parentNode.style.display = "none";      

                    document.getElementById( "divbusqueda" ).innerHTML =  "";            

                    obj.filtro = "";    
                    obj.main_list(obj, 1); 

                },
                false
            );     
            
            
        }  ,                        
        
        

        



        
        


    
    },




};





